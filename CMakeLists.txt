cmake_minimum_required(VERSION 2.8)
project( camera_app  )
#SET(COMPILE_DEFINITIONS -Werror)

#find_package( OpenCV REQUIRED )

add_executable(camera_app camera_app.cpp)
target_link_libraries(camera_app  /usr/lib/uv4l/uv4lext/armv6l/libuv4lext.so -lopencv_calib3d /usr/local/lib/libopencv_imgproc.so /usr/local/lib/libopencv_core.so /usr/local/lib/libopencv_highgui.so /usr/local/lib/libopencv_videoio.so /usr/local/lib/libopencv_video.so)
