#include "camera_app.h"

using namespace std;
using namespace cv;

void DetectMarkers(Mat  &frame)
{
	Mat edges, grey;
	cvtColor( frame, grey, CV_BGR2GRAY );
	Sobel( grey, edges, -1, 1, 1 );

	imshow("Output", edges);
}


int main(int argc, char** argv){
    clock_t start, end;

    VideoCapture cap(0);
    if (!cap.isOpened())
    {
        cout << "Cannot open camera" << endl;
        return -1;
    }
   cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
   cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

   namedWindow("Output",CV_WINDOW_AUTOSIZE);

    while (1)
    {
        start = clock() ;
        Mat frame;
        bool bSuccess = cap.read(frame);

        if (!bSuccess)
        {
        cout << "Cannot read a frame from camera" << endl;
        break;
        }

        if (waitKey(30) == 27)
        {
        cout << "Exit" << endl;
        break;
        }

	DetectMarkers(frame);

        end = clock() ;
	//printf ("seconds = %f\n", (end-start)/(double)CLOCKS_PER_SEC);
    }
    return 0;
}
